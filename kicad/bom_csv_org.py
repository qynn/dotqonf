#
#   Generates a Bill of Material in two formats...
#   + Tab delimited csv file
#   + org-mode formatted Table
#   ...based on a Kicad .xml net file
#
#   Usage : python3 pathToFile/bom_csv_org.py input.xml output.csv output.org qty
#
#   NB: use SPC SPC org-table-shrink in org-mode to fold columns

import csv
import sys
import netlist_reader
from bom_main import get_elements, open_file

net = netlist_reader.netlist(sys.argv[1])

qty = int(sys.argv[4]) # pass qty=0 to not check for prices

orgf = open_file(sys.argv[3])

csvf = open_file(sys.argv[2])
csvt = csv.writer(csvf, lineterminator='\n', delimiter=',', quotechar='\"', quoting=csv.QUOTE_ALL)

csvt.writerow(['Source:', net.getSource()])
csvt.writerow(['Date:', net.getDate()])
csvt.writerow(['Quantity:', qty])
csvt.writerow([' ', ' '])

if qty==0:
    csvt.writerow(['Ref', 'Qty', 'Value', 'Footprint', 'Link'])
    orgf.write('|------+-----+-------+-----------+------|\n')
    orgf.write('| <8>  | <3> |  <8>  |   <20>    | <12> |\n')
    orgf.write('| Refs | Qty | Value | Footprint | Link |\n')
    orgf.write('|------+-----+-------+-----------+------|\n')
else:
    csvt.writerow(['Ref', 'Qty(x1)', 'Value', 'Footprint', 'Link', 'Qty(x' + str(qty) + ')', '$CAD'])
    orgf.write('|------+---------+-------+-----------+------+----------+-----|\n')
    orgf.write('| <8>  |   <3>   |  <8>  |   <20>    | <12> |   <8>    | <8> |\n')
    orgf.write('| Refs | Qty(x1) | Value | Footprint | Link | Qty(x' + str(qty) + ') | $CAD | \n')
    orgf.write('|------+---------+-------+-----------+------+----------+-----|\n')

data = get_elements(net, qty)

for d in data:
    if qty==0:
        csvt.writerow([d['ref'], d['qty'], d['val'], d['ftprt'], d['link']])
        orgf.write('| ' + d['ref'] + ' | ' + d['qty'] + ' | ' + d['val'] + ' | ' + d['ftprt'] + ' | ' + d['link'] + ' |\n')
    else:
        csvt.writerow([d['ref'], d['qty'], d['val'], d['ftprt'], d['link'], d['total'], d['cad']])
        orgf.write('| ' + d['ref'] + ' | ' + d['qty'] + ' | ' + d['val'] + ' | ' + d['ftprt'] + ' | ' + d['link'] + ' |  ' + d['total'] + ' | '  + d['cad'] + ' |\n')

orgf.close()
