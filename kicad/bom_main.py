#    Bill Of Material generator plugin for kicad
#   Largely based on bom_csv_grouped_by_value_with_fp.py
#   Components are sorted by ref. and  grouped by value with same footprint
#   If a datasheet link is provided, it checks for Digikey's additional infos

import requests
import re
import validators
import sys

USE_SHORT_FOOTPRINTS = True
PULL_DIGIKEY_PRICES = True
PRINT_PRICE_TABLE = False

def open_file(file):
    try:
        f = open(file, 'w')
    except IOError:
        e = "Can't open output file w/ writing permissions " + file
        print(__file__, ":", e, sys.stderr)
        f = sys.stdout
    return f


def get_elements(net, qty): #pass null quantity to not check Digikey prices
    data = []
    totalCAD = 0
    missingCAD = 0
    # Get all of the components in groups of matching parts + values
    grouped = net.groupComponents()
    # (see ky_generic_netlist_reader.py)
    # Output all of the component information
    for group in grouped:
        refs = ""
        L = len(group)
        total = L*qty
        cad_un= 0
        link = "" #retained datasheet [[link]]
        url = "" #retained datasheet url
        link_r="; " #reference of retained datasheet
        # print("group: ", group,"(", L, ")")
        for i, c in enumerate(group):
            r = c.getRef()
            # lst = list(r)
            # vals = [d for d in list(r) if d.isdigit()]
            # print(r, lst, vals)
            refs += c.getRef()
            if (i < L-1):
                refs += ", "

            # Parsing datasheet
            datasheet = c.getDatasheet()
            if (datasheet != "~" and datasheet != ""):
                if (url != "" and datasheet != url):
                    print("W! INCONSISTENT DATASHEETS :", r, link_r)
                    print(datasheet, "vs", url)
                else:
                    link_r += r+"; "
                    url = datasheet
                    chunks = datasheet.split("/")
                    # there are different types of Digikey urls whether
                    # 1. product has been searched through filters
                    # https://www.digikey.ca/products/en?keywords=401-1966-ND
                    # 2. accessed directly via a keyword (e.g. part number)
                    # https://www.digikey.ca/products/en?keywords=401-1966-ND
                    # 3. a mix of the two (e.g. search for part number within filtered results)?
                    # https://www.digikey.ca/products/en/switches/pushbutton-switches/199?k=1966&pkeyword=&sv=0&sf=1&FV=69%7C411897%2C130%7C406606%2C-8%7C199%2C-1%7C108&quantity=&ColumnSort=0&page=1&pageSize=25 (please avoid these, slugs will end up in your table)

                    if (len(chunks) > 2):
                        if(chunks[2] == "www.digikey.ca"):
                            if (len(chunks) > 7): #1/#3
                                # manufacturer = chunks[5]
                                # product = chunks[6]
                                serial = chunks[7]
                            elif (len(chunks) == 5): #3
                                keyword = chunks[4]
                                serial = keyword.split("=")[1]
                            link = "[[" + datasheet + "][" + serial + "]]"
                            if PULL_DIGIKEY_PRICES & (qty > 0):
                                # print(datasheet, "(", r, ")")
                                cad_un = get_price(link_r, datasheet, total)
                    elif validators.url(link): #valid url
                        link = "[[" + datasheet + "][link]]"
                    else: #none or comment
                        link = datasheet

        # footprint is the same for all refs in group
        footprint = c.getFootprint()
        if USE_SHORT_FOOTPRINTS:
            footprint = footprint.split(':')[1] # removes footprint library
            footprint = footprint.replace("HandSoldering", "HS")
            footprint = footprint.replace("HandSolder", "HS")
            footprint = footprint.replace("Vertical", "Vert")
            footprint = footprint.replace("Horizontal", "Horz")
            chunks = footprint.split("_")
            footprint = ""
            for chunk in chunks:
                if ("Metric" not in chunk):
                    footprint += chunk + "_"
            footprint = footprint[:-1]

        dic = {}
        dic['ref'] = str(refs)
        dic['qty'] = str(L)
        dic['val'] = str(c.getValue())
        dic['ftprt'] = footprint
        dic['link'] = link
        if qty > 0:
            dic['total'] = str(total)
        if cad_un > 0:
            dic['cad_un'] = "{0:.4f}".format(cad_un)
            dic['cad'] = "{0:.2f}".format(total*cad_un)
            totalCAD += total*cad_un
        else:
            dic['cad'] = ''
            missingCAD += 1

        data.append(dic)

    if PULL_DIGIKEY_PRICES & (qty > 0):
        print("total: $CAD", "{0:.2f}".format(totalCAD), "(", missingCAD, "/", len(grouped), "missing cad_un )" )
    return data


# W! This worked with the older version of Digikey...
def get_price(ref, url, units):
    resp = requests.get(url)
    cad_un = 0

    # OLD Digikey Pricing Table format
    #     priceTable = resp.text.split('itemprop="price" id="schema-offer" >')[1].split("</table>")[0].replace(" ", "")
    #     unitPrice = priceTable.split('</span>')[0]
    #     priceTable = re.findall(r'<td>(.+?)</td>',priceTable)
    #     priceTable.insert(0, unitPrice)
    #     priceTable.insert(0, '1')

    try:
        priceTable = resp.text.split('data-testid="pricing-table">')[1].split("</table>")[0].replace("><", ">\n<")
        priceTable = re.findall(r'<td.*">(.+?)</td>', priceTable)
        priceTable = [p.replace(",", "") for p in priceTable]
        priceTable = [ [ int(priceTable[3*p]), float(priceTable[3*p+1].replace("$", ""))] for p in range(0, int(len(priceTable)/3))]

        for i in range(0, len(priceTable)):
            if ( priceTable[i][0] <= units ):
                cad_un = priceTable[i][1]
        if PRINT_PRICE_TABLE:
            print(ref, ":", url)
            print("priceTable ", priceTable)
            print("need", units, "units >> $CAD/un", cad_un, ">> $CAD", units*cad_un)
            print(" ")
    except Exception as e:
        print("W! could not get price for", ref, url)
        print("Exception:", e, "\n")
        cad_un = 666

    return cad_un

