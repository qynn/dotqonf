# # # VI MODE # # #
bindkey -v

# Remove mode switching delay.
KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
    if [[ ${KEYMAP} == vicmd ]] ||
           [[ $1 = 'block' ]]; then
        echo -ne '\e[1 q'

    elif [[ ${KEYMAP} == main ]] ||
             [[ ${KEYMAP} == viins ]] ||
             [[ ${KEYMAP} = '' ]] ||
             [[ $1 = 'beam' ]]; then
        echo -ne '\e[5 q'
    fi
}
zle -N zle-keymap-select

#enter insert mode on new prompt
_init_cursor() {
    echo -ne '\e[5 q'
}

precmd_functions+=(_init_cursor)

bindkey -M vicmd 'N' history-incremental-search-backward
bindkey '^R' history-incremental-pattern-search-backward
