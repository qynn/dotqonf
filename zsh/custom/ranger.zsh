# # $QONF/ranger/bookmarks wrapper # #


alias mark="$QONF/ranger/bookmarks"

alias m="$QONF/ranger/bookmarks add"

alias marks="$QONF/ranger/bookmarks list"


goto(){
    dir=$($QONF/ranger/bookmarks check $1)
    if [[ $? -eq 0 ]]; then
        cd $dir
    else
        echo $dir
    fi
}
