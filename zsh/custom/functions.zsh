#!/bin/bash

# # # FUNCTIONS # # #
# cd+ls
cs(){
    cd $1
    ls
}
# make current directory default
dflt(){
    echo new default directory: $PWD
    F=$HOME/.qonf/i3/dfltdir
    echo "export" DFLTDIR=$PWD > $F
}

#system upgrade (TODO move to debian.zsh)
aptu(){
    echo "***UPDATE***"
    sudo apt-get update
    echo "***UPGRADE***"
    sudo apt-get upgrade
    echo "***CLEAN***"
    sudo apt-get autoclean
    sudo apt-get autoremove
    echo "***DONE***"
}

#Mount Volume ($1=sdxi)
grab(){
    msg=$(udisksctl mount -b /dev/$1)
    mntpnt=$(echo ${msg} | cut -d ' ' -f 4) # matches volume label
    if [[ -n "$mntpnt" ]]; then
        cd $mntpnt
        return 0
    else
        return 1
    fi
}


ditch(){
    sdxi=/dev/$1
    # sdx=${sdxi::-1}
    sdx=${sdxi%?}
    mntd=$(df | grep -c $sdxi)

    if [ "$mntd" = "1" ]; then
	      udisksctl unmount -b $sdxi
    fi
        udisksctl power-off -b $sdx
}

# line to be added to /etc/fstab to mount without sudo:
# /dev/sdc1	/media/<username>/USBD	vfat	users,noauto,uid=<uid>,gid=<gid>	0	0
# <uid>: $ id -u <username>
# <gid>: $ id -g <username>

sdc1(){
    mntd=$(df | grep -c sdc1)
    if [ "$mntd" = "1" ]; then
	      udisksctl unmount -b /dev/sdc1
        udisksctl power-off -b /dev/sdc
    else
        msg=$(udisksctl mount -b /dev/sdc1)
        mntpnt=$(echo ${msg%?} | cut -d ' ' -f 4)
        cd $mntpnt
    fi
}

trash(){
    sudo rm -rv $HOME/.local/share/Trash/files/
    sudo rm -rv $HOME/.local/share/Trash/info/
    mkdir $HOME/.local/share/Trash/{files,info}
}
