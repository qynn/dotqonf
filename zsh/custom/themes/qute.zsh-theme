p_status() {
  # echo "%(?.%F{cyan}→%f.%K{magenta}%F{white}X%f%k)"
  echo "%(?.%F{cyan}::%f.%K{magenta}%F{white}X%f%k)"
}

p_username() {
  echo "%F{red}%n"
}

p_directory() {
  echo "%F{red}%9~"
}

p_git() {
  # echo "%K{green}%F{blue}$(git_prompt_info)%f%k"
  echo "%F{yellow}$(git_prompt_info)%f"
}

# set the git_prompt_info text
ZSH_THEME_GIT_PROMPT_PREFIX="["
ZSH_THEME_GIT_PROMPT_SUFFIX="]"
ZSH_THEME_GIT_PROMPT_DIRTY="*"
ZSH_THEME_GIT_PROMPT_CLEAN=""

# putting it all together
PROMPT='%B$(p_directory)%b$(p_git) $(p_status) '
# RPROMPT='$(p_status)'
