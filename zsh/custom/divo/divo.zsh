# # # DIVO (void) # # #

cdpath=($HOME $QONF $HOME/Web $HOME/Prog)

## Outputs ##
xrandr --output HDMI1 --auto --right-of HDMI2

## XBPS ##
# upgrade packages
alias xu='sudo xbps-install -Suv'
# install package(s)
# see xi utility from xtools
alias xI='sudo xbps-install'
# remove package(s)
alias xr='sudo xbps-remove -R'
# remove old and orphan packages
alias xo='sudo xbps-remove -oOv'
# query repositories
alias xq='xbps-query -Rs'
# list installed packages
alias xl='xbps-query -l'
# manually installed packages
alias xm='xbps-query -m'
# list dependencies
alias xx='xbps-query -x'
# list reverse dependencies
alias xX='xbps-query -X'

## Network ##
alias sshON='sudo sv up sshd'
alias sshOFF='sudo sv down sshd'
alias vnc='$HOME/.vncviewer'
alias vpn='sudo protonvpn -f c'

## System ##
alias dmesg='sudo dmesg'
# see /etc/sudoers
alias shut='sudo /usr/bin/poweroff'
alias reboot='sudo /usr/bin/reboot'

## Misc ##
alias unrar='bsdtar -xf'
alias scan='utsushi scan'

## Fun ##
alias playmidi='fluidsynth -a alsa -l -i /usr/share/soundfonts/FluidR3_GM.sf2'


midi2mp3(){
    echo $1
    fluidsynth -a alsa -l -T raw -F - /usr/share/soundfonts/FluidR3_GM.sf2 $1 \
      | twolame -b 256 -r - ${1%.mid}.mp3
}

drain(){
    cd ~/Web/drained/
    source venv/bin/activate
}

backup(){
    read "yn?rsync: $HOME > $BCKP (Warning: --delete flag is set!) Are you sure? [Y/n] "
    if [[ ! "$yn" =~ ^[Yy]$ ]]; then
        echo "Aborting"
    else
        sudo rsync -av --delete --exclude 'Dwnld' --exclude '.local/share/Trash' --exclude '.cache' --info=progress2 $HOME $BCKP
    fi
}
