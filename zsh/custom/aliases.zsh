# # # ALIASES  # # #

# better defaults
alias grep='grep --color=auto'

# short shorts
alias e='emacsclient --no-wait'
alias E='SUDO_EDITOR=\"emacsclient\" sudoedit'
alias X='exit'
alias R='ranger'
alias M='mkdir'
alias F='fzf'
alias C='clear'
alias G='grep --ignore-case'

# short cuts
alias lf='sudo find / -name'
alias chq='sudo chown $USER'
alias crone='VISUAL=vim crontab -e'
alias venv='source venv/bin/activate'
alias bash='urxvt -e /bin/bash'
alias xcolor='$QONF/X11/colortest'
alias xdpi='$QONF/utils/zoom'

alias py='python3'
alias pip='noglob python3 -m pip'
alias gpg='gpg2'

alias nm-restart='sudo service network-manager restart'
alias syslog='less /var/log/syslog'
alias minimon='sudo minicom -c on -D /dev/ttyACM0'
alias gitmode='git config core.filemode false'

# software start
alias kee='keepass2'
alias web='firefox --private-window'
alias calc='libreoffice --calc'

alias wtf='figlet -f script "what the fuck !"'

alias glow='glow -s $QONF/glow/pink.json'
