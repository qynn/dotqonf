# # # lube (untu) # # #

cdpath=($HOME $QONF)

## Inputs ##
# Trackpad
alias padOFF=' xinput set-prop "Synaptics TM3149-002" "Device Enabled" 0'
alias padON=' xinput set-prop "Synaptics TM3149-002" "Device Enabled" 1'
# slow down TrackPoint
xinput set-prop "TPPS/2 IBM TrackPoint" "libinput Accel Speed" -0.6
# xinput set-prop "Synaptics TM3149-002" "Device Enabled" 0

## Network ##
alias wifi='nmcli con up \*nixp\*rnrules'
alias ethos='nmcli con up ethos'
alias ethON='nmcli dev connect enp0s31f6'
alias ethOFF='nmcli dev disconnect enp0s31f6'
alias wifiON='nmcli dev connect wlp4s0'
alias wifiOFF='nmcli dev disconnect wlp4s0'

## Misc ##
alias reboot='sudo shutdown -r now'
alias fd='fdfind'
