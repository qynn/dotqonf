# # #  APT # # #

alias aptg='sudo apt install'
alias aptr='sudo apt remove'
alias aptR='sudo apt-get purge'
alias aptu='sudo apt update'
alias aptU='sudo apt update && sudo apt upgrade'
alias aptc='sudo apt-get autoclean && sudo apt autoremove'
alias aptl='apt list'
alias apts='apt-cache search'
alias aptd='apt-cache depends'
alias aptp='apt-cache policy'
alias aptm='apt-mark showmanual'
