cdpath=($HOME/Rbct/Projets $HOME/Rbct/RetD)

alias pd='purr-data'
alias touchEditor='java -jar $HOME/Rbct/RetD/OSControl/Editor/TouchOSCEditor/TouchOSCEditor.jar &'
# alias log='libreoffice --calc ~/Rbct/Interne/Invoices/log.ods'

alias gitmode='git config core.filemode false'

# VNC
alias tight='xtightvncviewer 192.168.2.26 -autopass'

