(defun delete-ranger-bookmarks ()
  (interactive)
  (dolist (bookmark bookmark-alist)
    (if (eq (string-match "^ranger-" (car bookmark)) 0)
        ;; (message "deleting %s" (car bookmark))
        (bookmark-delete bookmark)
    )
  )
)


(defun load-ranger-bookmark (line)
  (setq mark (split-string line ":"))
  (setq key (format "ranger-%s" (car mark)))
  (setq record `((filename . ,(car (cdr mark)))))
  ;; (message "loading %s = %s\n" key record)
  ;; TODO add no-overwrite switch (default nil)
  (bookmark-store key record nil)
)


(defun parse-ranger-bookmarks-file (file)
  (message "Loading bookmarks from %s" file)
  (with-temp-buffer (insert-file-contents file)
                    (mapcar 'load-ranger-bookmark
                            (split-string (buffer-string) "\n" t))
  )
)


(defun ranger-with-bookmarks (&optional file)
  (interactive)
  (unless file (setq file "~/.local/share/ranger/bookmarks"))
  (delete-ranger-bookmarks)
  (parse-ranger-bookmarks-file file)
  (ranger)
)
